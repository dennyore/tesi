<?php
    @require('connessioneDB.php');
    // Oggetto da restituire
    $toReturn = array();
    $toReturn['stato'] = "Ok";
    $toReturn['messaggio'] = "";

    $connection = getDBConnection();
    if($connection == null){
        $toReturn['stato'] = "Errore";
        $toReturn['messaggio'] = "Connessione al DataBase non riuscita";
        die(json_encode($toReturn));
    }
    $query = $connection->query("SELECT * FROM beacon");
    if(null==$query){
        $toReturn['stato'] = "Errore";
        $toReturn['messaggio'] = "Impossibile recuperare la lista dei beacons.";
        //echo(json_encode($toReturn));
    }
    // Campi tabella DB: MAC Nome Descrizione Lat Lng Edificio Piano
    $indice = 0;
    while($riga = $query->fetch_row()){
        $toReturn['beacons'][$indice]['MAC'] = $riga[0];
        $toReturn['beacons'][$indice]['Nome'] = $riga[1];
        $toReturn['beacons'][$indice]['Descrizione'] = $riga[2];
        $toReturn['beacons'][$indice]['Lat'] = $riga[3];
        $toReturn['beacons'][$indice]['Lng'] = $riga[4];
        $toReturn['beacons'][$indice]['Edificio'] = $riga[5];
        $toReturn['beacons'][$indice]['Piano'] = $riga[6];
        $queryPagine = $connection->query("SELECT URL FROM mostra WHERE MAC='$riga[0]'");
        $indicePagine = 0;
        $toReturn['beacons'][$indice]['Pagine'] = array();
        while($rigaPagine = $queryPagine->fetch_row()){
            $toReturn['beacons'][$indice]['Pagine'][$indicePagine++] = $rigaPagine[0];
        }
        $indice++;
    }
    echo(json_encode($toReturn));
    $connection->close();
?>