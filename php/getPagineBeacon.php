<?php
    @require('connessioneDB.php');
    if(!isset($_POST['MAC'])){
        die('Errore: MAC non definito.');
    }
    $MAC = $_POST['MAC'];
    $files = array();
    
    // get connections
    $connection = getDBConnection();
    if($connection==null){
        $datiUtente['stato'] = "Errore";
        $datiUtente['messaggio'] = "Impossibile connettersi al db.";
        die(json_encode($datiUtente));
    }

    $query = $connection->query("SELECT URL FROM mostra WHERE mostra.MAC='$MAC'");
    if($query == FALSE){
        $datiUtente['stato'] = "Errore";
        $datiUtente['messaggio'] = "Impossibile recuperare le pagine associate al beacon.";
        die(json_encode($datiUtente));
    }

    $i = 0;
    while($result = $query->fetch_row()){
        $files[$i++] = $result[0];
    }

    echo json_encode($files);

    // TODO: Da rimuovere nel prossimo commit
    /*
    if(!file_exists('./pagine/'.$MAC)){
        die('Errore: Cartella non trovata.');
    }

    $files = null;

    if($handle = opendir('./pagine/'.$MAC)){
        while($file = readdir($handle)){
            if(!is_dir('./pagine/'.$MAC.'/'.$file)){
                if ($file != "." & $file != "..") $files[] = $file;
            }
        }
    }
    closedir($handle);*/
?>