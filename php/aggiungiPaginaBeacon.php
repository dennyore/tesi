<?php
    @require('connessioneDB.php');
    // Oggetto da restituire
    $toReturn = array();
    $toReturn['stato'] = "Ok";
    $toReturn['messaggio'] = "";

    if(!isset($_POST['MAC'])){
        $toReturn['stato'] = "Errore";
        $toReturn['messaggio'] = "MAC non settato.";
        die(json_encode($toReturn));
    }
    if(!isset($_POST['Pagina'])){
        $toReturn['stato'] = "Errore";
        $toReturn['messaggio'] = "Pagina non settata.";
        die(json_encode($toReturn));
    }

    $MAC = $_POST['MAC'];
    $Pagina = $_POST['Pagina'];

    $connection = getDBConnection();
    if($connection == null){
        $toReturn['stato'] = "Errore";
        $toReturn['messaggio'] = "Connessione al DataBase non riuscita";
        die(json_encode($toReturn));
    }

    $query = $connection->query("INSERT INTO pagina VALUES('$Pagina')");
    if(null==$query){
        $toReturn['stato'] = "Errore";
        $toReturn['messaggio'] = "Impossibile aggiungere la nuova pagina.";
        $connection->close();
        die(json_encode($toReturn));
        //die(json_encode($toReturn));
    }

    $query = $connection->query("INSERT INTO mostra VALUES('$Pagina','$MAC')");
    if(null==$query){
        $toReturn['stato'] = "Errore";
        $toReturn['messaggio'] = "Impossibile associare la nuova pagina col beacon.";
        //die(json_encode($toReturn));
    }
    
    echo json_encode($toReturn);
    $connection->close();
?>