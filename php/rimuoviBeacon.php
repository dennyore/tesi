<?php
    @require('connessioneDB.php');
    // Oggetto da restituire
    $toReturn = array();
    $toReturn['stato'] = "Ok";
    $toReturn['messaggio'] = "";

    // Controllo se sono stati ricevuti tutti i parametri
    if(!isset($_POST['MAC'])){
        $toReturn['stato'] = "Errore";
        $toReturn['messaggio'] = "MAC non ricevuto.";
    }

    $MAC = $_POST['MAC'];

    $connection = getDBConnection();
    if($connection == null){
        $toReturn['stato'] = "Errore";
        $toReturn['messaggio'] = "Connessione al DataBase non riuscita";
        die(json_encode($toReturn));
    }
    
    $query = $connection->query("DELETE FROM beacon WHERE beacon.MAC='$MAC'");
    if(null==$query){
        $toReturn['stato'] = "Errore";
        $toReturn['messaggio'] = "Impossibile eseguire la cancellazione del beacon.";
    }

    echo json_encode($toReturn);
    $connection->close();

    // TODO: Eliminare la cartella quando viene eliminato un beacon

    // Elimino la cartella del beacon
    /*$cartella = str_replace(':','',$MAC);
    if (file_exists("./pagine/$cartella")) {
        // Elimino tutti i file all'interno della cartella
        $files = glob("./pagine/$cartella/{,.}*", GLOB_BRACE);
        foreach($files as $file){
            if(is_file($file)){
                unlink($file);
            }
        }
        rmdir("./pagine/$cartella");
    }*/

    // FIXME: Non elimina la cartella. Viene eliminato solo il beacon
    /*function deleteDir($dirPath) {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    deleteDir("./pagine/".str_replace(':','',$MAC)."/");*/
?>